import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:goodhabit/pages/homepage.dart';
import 'package:goodhabit/pages/navigation/navigation.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      scrollBehavior: MaterialScrollBehavior().copyWith(
        dragDevices: {
          PointerDeviceKind.mouse,
          PointerDeviceKind.touch,
          PointerDeviceKind.stylus,
          PointerDeviceKind.unknown
        },
      ),
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
            seedColor: const Color(0xff3843FF)),
        useMaterial3: true,
      ),
      home: const homepage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
