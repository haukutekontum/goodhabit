import 'package:flutter/material.dart';
import 'package:goodhabit/pages/navigation/compass.dart';
import 'package:goodhabit/pages/navigation/home.dart';
import 'package:goodhabit/pages/navigation/person.dart';
import 'package:goodhabit/pages/navigation/plus.dart';
import 'package:goodhabit/pages/navigation/reward.dart';

class navigation extends StatefulWidget {
  const navigation({super.key});

  @override
  State<navigation> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<navigation> {
  int _currentIndex = 2;
  List<Widget> body = const [
    home(),
    compass(),
    plus(),
    reward(),
    person(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: body[_currentIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        unselectedItemColor: Colors.black26,
        selectedItemColor: const Color(0xff3843FF),
        onTap: (int newIndex) {
          setState(() {
            _currentIndex = newIndex;
          });
        },
        items: const [
          BottomNavigationBarItem(
              icon: ImageIcon(AssetImage(
                  "assets/images/icon_home.png")),
              label: "home"),
          BottomNavigationBarItem(
              icon: ImageIcon(AssetImage(
                  "assets/images/icon_compass.png")),
              label: "compass"),
          BottomNavigationBarItem(
              icon: ImageIcon(AssetImage(
                  "assets/images/icon_plus.png")),
              label: "plus"),
          BottomNavigationBarItem(
              icon: ImageIcon(AssetImage(
                  "assets/images/icon_reward.png")),
              label: "reward"),
          BottomNavigationBarItem(
              icon: ImageIcon(AssetImage(
                  "assets/images/icon_person.png")),
              label: "person"),
        ],
      ),
    );
  }
}
