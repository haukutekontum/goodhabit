import 'package:flutter/material.dart';
import 'package:goodhabit/pages/lists/challenges.dart';
import 'package:goodhabit/pages/lists/habit_clubs.dart';
import 'package:goodhabit/pages/lists/learning.dart';
import 'package:goodhabit/pages/lists/suggested_list.dart';

class plus extends StatefulWidget {
  const plus({super.key});

  @override
  State<plus> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<plus> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: const Color(0xFFF6F9FF),
            body: SingleChildScrollView(
              child: Column(children: [
                Container(
                  color: Colors.white,
                  padding: EdgeInsets.only(
                      right: 20,
                      left: 20,
                      top: 15,
                      bottom: 15),
                  alignment: Alignment.center,
                  child: Row(
                      mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          "Explore",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 27,
                              fontWeight: FontWeight.bold),
                        ),
                        Container(
                          padding: EdgeInsets.all(13),
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              border: Border.all(
                                  color: Colors.black12,
                                  width: 1),
                              borderRadius:
                                  BorderRadius.circular(
                                      15)),
                          child: const Icon(
                            Icons.search,
                            color: Colors.black,
                            size: 21,
                          ),
                        )
                      ]),
                ),
                Container(
                  padding: const EdgeInsets.only(
                      top: 10,
                      bottom: 10,
                      left: 20,
                      right: 20),
                  child: const Row(
                      mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Suggested for You",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.bold),
                        ),
                        Text(
                          "VIEW ALL",
                          style: TextStyle(
                              color: Color(0xff3843FF),
                              fontSize: 12,
                              fontWeight: FontWeight.bold),
                        ),
                      ]),
                ),
                suggested_list(),
                SizedBox(
                  height: 5,
                ),
                Container(
                  padding: const EdgeInsets.only(
                      top: 10,
                      bottom: 10,
                      left: 20,
                      right: 20),
                  child: const Row(
                      mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Habit Clubs",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.bold),
                        ),
                        Text(
                          "VIEW ALL",
                          style: TextStyle(
                              color: Color(0xff3843FF),
                              fontSize: 12,
                              fontWeight: FontWeight.bold),
                        ),
                      ]),
                ),
                habit_clubs(),
                const SizedBox(
                  height: 5,
                ),
                Container(
                  padding: const EdgeInsets.only(
                      top: 10,
                      bottom: 10,
                      left: 20,
                      right: 20),
                  child: const Row(
                      mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Challenges",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.bold),
                        ),
                        Text(
                          "VIEW ALL",
                          style: TextStyle(
                              color: Color(0xff3843FF),
                              fontSize: 12,
                              fontWeight: FontWeight.bold),
                        ),
                      ]),
                ),
                challenges(),
                const SizedBox(
                  height: 5,
                ),
                Container(
                  padding: const EdgeInsets.only(
                      top: 10,
                      bottom: 10,
                      left: 20,
                      right: 20),
                  child: const Row(
                      mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Learning",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.bold),
                        ),
                        Text(
                          "VIEW ALL",
                          style: TextStyle(
                              color: Color(0xff3843FF),
                              fontSize: 12,
                              fontWeight: FontWeight.bold),
                        ),
                      ]),
                ),
                learning(),
              ]),
            )));
  }
}
