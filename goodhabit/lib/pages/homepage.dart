import 'package:flutter/material.dart';
import 'package:goodhabit/pages/login.dart';

class homepage extends StatefulWidget {
  const homepage({super.key});

  @override
  State<homepage> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<homepage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                    "assets/images/background.png"),
                fit: BoxFit.cover)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            SizedBox(
              height: 350,
              width: double.infinity,
              child: Container(
                margin: const EdgeInsets.only(left: 100),
                child: const DecoratedBox(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                                "assets/images/image1.png"),
                            fit: BoxFit.cover))),
              ),
            ),
            SizedBox(
              child: Container(
                margin: EdgeInsets.only(left: 30.0),
                width: double.infinity,
                child: const Text(
                  "Create",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 35,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1.5),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
            SizedBox(
                child: Container(
              margin: EdgeInsets.only(left: 30.0),
              width: double.infinity,
              child: const Text(
                "Good Habits",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1.5),
                textAlign: TextAlign.left,
              ),
            )),
            SizedBox(
              child: Container(
                margin: const EdgeInsets.only(
                    left: 30.0, top: 5.0),
                width: double.infinity,
                child: const Text(
                  "Change your life by slowly adding new healthy habits and sticking to them",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.w100,
                      fontFamily: 'RobotoThin',
                      letterSpacing: 1),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
            Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius:
                        BorderRadius.circular(40)),
                padding: const EdgeInsets.all(18),
                margin: const EdgeInsets.only(
                    top: 50, right: 30, left: 30),
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                const login()));
                  },
                  child: Row(
                    mainAxisAlignment:
                        MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Image.asset(
                        'assets/images/icon_next.png',
                        width: 17,
                        height: 17,
                        fit: BoxFit.contain,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      const Text(
                        "Continue with E-mail",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15,
                          fontWeight: FontWeight.w100,
                          fontFamily: 'RobotoThin',
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ],
                  ),
                )),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  flex: 1,
                  child: Container(
                    padding: const EdgeInsets.only(
                        top: 7,
                        bottom: 7,
                        left: 12,
                        right: 12),
                    margin: const EdgeInsets.only(
                        top: 15,
                        bottom: 15,
                        left: 18,
                        right: 18),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                            BorderRadius.circular(30)),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment:
                          MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/icon_iphone.png',
                          fit: BoxFit.contain,
                          width: 15,
                          height: 15,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        const Text(
                          'Apple',
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.black,
                              fontWeight:
                                  FontWeight.normal),
                        )
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.only(
                        top: 7,
                        bottom: 7,
                        left: 12,
                        right: 12),
                    margin: const EdgeInsets.only(
                        top: 15,
                        bottom: 15,
                        left: 18,
                        right: 18),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                            BorderRadius.circular(30)),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment:
                          MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/icon_google.png',
                          fit: BoxFit.contain,
                          width: 15,
                          height: 15,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        const Text(
                          'Google',
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.black,
                              fontWeight:
                                  FontWeight.normal),
                        )
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.only(
                        top: 7,
                        bottom: 7,
                        left: 12,
                        right: 12),
                    margin: const EdgeInsets.only(
                        top: 15,
                        bottom: 15,
                        left: 18,
                        right: 18),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                            BorderRadius.circular(30)),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment:
                          MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/icon_facebook.png',
                          fit: BoxFit.contain,
                          width: 15,
                          height: 15,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        const Text(
                          'Facebook',
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.black,
                              fontWeight:
                                  FontWeight.normal),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const Text(
              "By continuing you agree Terms of Services & Privacy Policy",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.w100),
            )
          ],
        ),
      ),
    ));
  }
}
