import 'dart:html';

import 'package:flutter/material.dart';
import 'package:goodhabit/pages/createaccount.dart';
import 'package:goodhabit/pages/homepage.dart';
import 'package:validators/validators.dart';

class login extends StatefulWidget {
  const login({super.key});

  @override
  State<login> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<login> {
  get style => null;
  final textEditingController = TextEditingController();
  bool isEmailCorrect = false;
  bool isTextFieldChange = false;

  @override
  void dispose() {
    super.dispose();
    textEditingController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: const Color(0xFFF6F9FF),
          body: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                color: Colors.white,
                padding: const EdgeInsets.all(20),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment:
                      MainAxisAlignment.center,
                  children: [
                    Container(
                      transform: Matrix4.translationValues(
                          -100.0, 0.0, 0.0),
                      child: InkWell(
                        child: Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                borderRadius:
                                    BorderRadius.circular(
                                        10),
                                border: Border.all(
                                  width: 1,
                                  color: Colors.black26,
                                )),
                            child: InkWell(
                              onTap: () =>
                                  {Navigator.pop(context)},
                              child: const Icon(
                                Icons.arrow_back_ios,
                                color: Colors.black,
                                size: 14,
                              ),
                            )),
                        onTap: () {},
                      ),
                    ),
                    const Text(
                      "Continue with E-mail",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 27,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(
                    left: 30,
                    top: 30,
                    bottom: 30,
                    right: 50),
                child: Column(
                  mainAxisAlignment:
                      MainAxisAlignment.start,
                  crossAxisAlignment:
                      CrossAxisAlignment.start,
                  children: [
                    const Text(
                      "E-MAIL",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 13,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    TextField(
                      controller: textEditingController,
                      onChanged: (text) {
                        setState(() {
                          isEmailCorrect = isEmail(text);
                        });
                      },
                      onTap: () => {
                        setState(() {
                          isTextFieldChange = true;
                        })
                      },
                      style: const TextStyle(fontSize: 15),
                      decoration: InputDecoration(
                          hintText: 'Enter your email',
                          hintStyle: const TextStyle(
                              fontSize: 15,
                              color: Colors.black26),
                          suffixIcon: isEmailCorrect ==
                                  false
                              ? InkWell(
                                  child: Icon(
                                    Icons.close,
                                    color:
                                        isTextFieldChange ==
                                                false
                                            ? Colors.black26
                                            : Colors.red,
                                    size: 15,
                                  ),
                                  onTap: () {
                                    setState(() {
                                      textEditingController
                                          .clear();
                                    });
                                  },
                                )
                              : const Icon(
                                  Icons.check,
                                  color: Colors.green,
                                  size: 15,
                                ),
                          enabledBorder:
                              const UnderlineInputBorder(
                                  borderSide: BorderSide(
                            color: Color(0xff3843FF),
                          )),
                          focusedBorder:
                              UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          isEmailCorrect ==
                                                  false
                                              ? Colors.red
                                              : Colors
                                                  .green))),
                      cursorColor: isEmailCorrect == false
                          ? Colors.red
                          : Colors.green,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const Text(
                      "PASSWORD",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 13,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    const TextField(
                      style: TextStyle(fontSize: 15),
                      obscureText: true,
                      decoration: InputDecoration(
                          hintText: 'Enter your password',
                          hintStyle: TextStyle(
                              fontSize: 15,
                              color: Colors.black26),
                          suffixIcon: Icon(
                            Icons.close,
                            color: Colors.black26,
                            size: 15,
                          )),
                    ),
                    const SizedBox(
                      height: 19,
                    ),
                    const Text(
                      "I forgot my password",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 15,
                          fontWeight: FontWeight.normal),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 40,
              ),
              Column(
                crossAxisAlignment:
                    CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "Don't have account? Let's create!",
                    style: TextStyle(
                        color: Color(0xff3843FF),
                        fontSize: 15,
                        fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  SizedBox(
                    width: 350,
                    height: 50,
                    child: TextButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    const createAcount()));
                      },
                      style: TextButton.styleFrom(
                          backgroundColor:
                              const Color(0xff3843FF)),
                      child: const Text("Next",
                          style: TextStyle(
                              fontSize: 16,
                              color: Colors.white)),
                    ),
                  ),
                ],
              )
            ],
          )),
    );
  }
}
