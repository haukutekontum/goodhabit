import 'package:flutter/material.dart';
import 'package:goodhabit/pages/navigation/navigation.dart';

class createAcount extends StatefulWidget {
  const createAcount({super.key});

  @override
  State<createAcount> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<createAcount> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: const Color(0xFFF6F9FF),
            body: Column(children: [
              Expanded(
                flex: 1,
                child: Container(
                  color: Colors.white,
                  padding: const EdgeInsets.all(20),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment:
                        MainAxisAlignment.center,
                    children: [
                      Container(
                        transform:
                            Matrix4.translationValues(
                                -100.0, 0.0, 0.0),
                        child: InkWell(
                          child: Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  borderRadius:
                                      BorderRadius.circular(
                                          10),
                                  border: Border.all(
                                    width: 1,
                                    color: Colors.black26,
                                  )),
                              child: InkWell(
                                onTap: () => {
                                  Navigator.pop(context)
                                },
                                child: const Icon(
                                  Icons.arrow_back_ios,
                                  color: Colors.black,
                                  size: 14,
                                ),
                              )),
                          onTap: () {},
                        ),
                      ),
                      const Text(
                        "Create Account",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 27,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                  flex: 7,
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.topLeft,
                        margin: const EdgeInsets.only(
                            top: 30, left: 30),
                        child: const Text(
                          "Choose your gender",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 17,
                              fontWeight:
                                  FontWeight.normal),
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Container(
                              padding:
                                  const EdgeInsets.only(
                                      top: 40,
                                      bottom: 40,
                                      left: 65,
                                      right: 65),
                              margin: const EdgeInsets.only(
                                  right: 15,
                                  left: 30,
                                  bottom: 30,
                                  top: 30),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      const BorderRadius
                                          .all(
                                          Radius.circular(
                                              20)),
                                  border: Border.all(
                                      color: Colors.black12,
                                      width: 1)),
                              child: const Column(
                                children: [
                                  Image(
                                      image: AssetImage(
                                          'assets/images/boyicon.png')),
                                  Text(
                                    "Male",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 15,
                                        fontWeight:
                                            FontWeight
                                                .normal),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                              flex: 1,
                              child: Container(
                                padding:
                                    const EdgeInsets.only(
                                        top: 40,
                                        bottom: 40,
                                        left: 65,
                                        right: 65),
                                margin:
                                    const EdgeInsets.only(
                                        right: 30,
                                        left: 15,
                                        top: 30,
                                        bottom: 30),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                        const BorderRadius
                                            .all(
                                            Radius.circular(
                                                20)),
                                    border: Border.all(
                                        color:
                                            Colors.black12,
                                        width: 1)),
                                child: const Column(
                                  children: [
                                    Image(
                                        image: AssetImage(
                                            'assets/images/girlicon.png')),
                                    Text(
                                      "Female",
                                      style: TextStyle(
                                          color:
                                              Colors.black,
                                          fontSize: 15,
                                          fontWeight:
                                              FontWeight
                                                  .normal),
                                    ),
                                  ],
                                ),
                              ))
                        ],
                      ),
                    ],
                  )),
              Flexible(
                flex: 1,
                child: Container(
                  width: double.infinity,
                  height: 50,
                  margin: const EdgeInsets.only(
                      left: 20, right: 20),
                  child: TextButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  const navigation()));
                    },
                    style: TextButton.styleFrom(
                        backgroundColor:
                            const Color(0xff3843FF)),
                    child: const Text("Next",
                        style: TextStyle(
                            fontSize: 16,
                            color: Colors.white)),
                  ),
                ),
              )
            ])));
  }
}
