import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class challenges extends StatefulWidget {
  challenges({super.key});

  List<peopleJoined> items = [
    peopleJoined(
        title: "Best Runners!",
        subtitle: "5 days 13 hours left",
        percent: 0.3,
        listPeople: [
          people(urlImage: "assets/images/avatar2.png"),
          people(urlImage: "assets/images/avatar1.png")
        ]),
    peopleJoined(
        title: "Best Bikers!",
        subtitle: "2 days 11 hours left",
        percent: 0.8,
        listPeople: [
          people(urlImage: "assets/images/avatar2.png"),
        ]),
  ];

  @override
  State<challenges> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<challenges> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
      margin: const EdgeInsets.only(left: 10, right: 10),
      height: 160,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: widget.items.length,
          itemBuilder: (context, index) =>
              buildCard(item: widget.items[index])),
    ));
    ;
  }

  Widget buildCard({required peopleJoined item}) =>
      Container(
          padding: const EdgeInsets.only(
              top: 13, left: 20, right: 20),
          margin: const EdgeInsets.only(left: 7, right: 7),
          width: 250,
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color(0xFF6B73FF),
                    Color(0xFF000DFF)
                  ]),
              borderRadius:
                  BorderRadius.all(Radius.circular(20))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Image(
                image: AssetImage(
                    "assets/images/icon_clock.png"),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                item.title.toString().trim(),
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 1,
              ),
              Text(
                item.subtitle.toString().trim(),
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 13,
                    fontWeight: FontWeight.normal),
              ),
              const SizedBox(
                height: 7,
              ),
              LinearPercentIndicator(
                  width: 210.0,
                  lineHeight: 8.0,
                  percent: item.percent,
                  barRadius: const Radius.circular(5),
                  progressColor: Colors.white),
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  overlapped(people: item.listPeople),
                  const SizedBox(
                    width: 3,
                  ),
                  Text(
                    item.listPeople.length
                            .toString()
                            .trim() +
                        " friends joined",
                    style: const TextStyle(
                        color: Colors.white,
                        fontSize: 11,
                        fontWeight: FontWeight.normal),
                  ),
                ],
              )
            ],
          ));
}

Widget overlapped({required List<people> people}) {
  final overlap = 12.0;

  List<Widget> stackLayers =
      List<Widget>.generate(people.length, (index) {
    return Padding(
      padding: EdgeInsets.fromLTRB(
          index.toDouble() * overlap, 0, 0, 0),
      child: Image.asset(
        people[index].urlImage.toString().trim(),
        width: 24,
        height: 24,
      ),
    );
  });

  return Stack(children: stackLayers);
}

class peopleJoined {
  final String title;
  final String subtitle;
  final double percent;
  final List<people> listPeople;

  peopleJoined(
      {required this.title,
      required this.subtitle,
      required this.percent,
      required this.listPeople});
}

class people {
  final String urlImage;
  people({required this.urlImage});
}
