import 'package:flutter/material.dart';

class habit_clubs extends StatefulWidget {
  habit_clubs({super.key});

  List<Habits> items = [
    Habits(
        urlImage: "assets/images/activity4.png",
        title: "Cat Lovers",
        subtitle: "462 members"),
    Habits(
        urlImage: "assets/images/activity5.png",
        title: "Istanbul",
        subtitle: "+500 members"),
    Habits(
        urlImage: "assets/images/activity6.png",
        title: "Runners",
        subtitle: "336 members"),
  ];

  @override
  State<habit_clubs> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<habit_clubs> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
      margin: const EdgeInsets.only(left: 10, right: 10),
      height: 110,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: widget.items.length,
          itemBuilder: (context, index) =>
              buildCard(item: widget.items[index])),
    ));
  }
}

Widget buildCard({required Habits item}) => Container(
      padding: const EdgeInsets.only(
          top: 10, left: 20, right: 20),
      margin: const EdgeInsets.only(left: 7, right: 7),
      width: 170,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius:
              const BorderRadius.all(Radius.circular(20)),
          border: Border.all(color: Colors.black12)),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset(
              item.urlImage.toString().trim(),
              fit: BoxFit.fill,
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              item.title.toString().trim(),
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.normal),
            ),
            const SizedBox(
              height: 1,
            ),
            Text(
              item.subtitle.toString().trim(),
              style: const TextStyle(
                  color: Colors.black45,
                  fontSize: 13,
                  fontWeight: FontWeight.normal),
            ),
          ]),
    );

class Habits {
  final String urlImage;
  final String title;
  final String subtitle;

  Habits(
      {required this.urlImage,
      required this.title,
      required this.subtitle});
}
