import 'package:flutter/material.dart';

class learning extends StatefulWidget {
  learning({super.key});
  List<learningItem> learningList = [
    learningItem(
        imageUrl: "assets/images/image2.png",
        title: "Why should we drink water often?"),
    learningItem(
        imageUrl: "assets/images/image3.png",
        title: "Benefits of regular walking")
  ];

  @override
  State<learning> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<learning> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
      margin: const EdgeInsets.only(left: 10, right: 10),
      height: 160,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: widget.learningList.length,
        itemBuilder: (context, index) =>
            buildCard(item: widget.learningList[index]),
      ),
    ));
  }
}

Widget buildCard({required learningItem item}) => Container(
    // padding:
    //     const EdgeInsets.only(top: 13, left: 20, right: 20),
    margin: const EdgeInsets.only(left: 7, right: 7),
    width: 250,
    decoration: const BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [Color(0xFF6B73FF), Color(0xFF000DFF)]),
        borderRadius:
            BorderRadius.all(Radius.circular(20))),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Image.asset(
          item.imageUrl.toString().trim(),
          width: double.maxFinite,
          height: 90,
          fit: BoxFit.fill,
        ),
        Container(
          padding: const EdgeInsets.all(10),
          child: Row(
            children: [
              const Image(
                image: AssetImage(
                    "assets/images/icon_file.png"),
              ),
              const SizedBox(
                height: 10,
              ),
              const SizedBox(
                width: 10,
              ),
              Flexible(
                child: Text(
                  item.title.toString().trim(),
                  style: const TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.normal),
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  softWrap: false,
                ),
              )
            ],
          ),
        )
      ],
    ));

class learningItem {
  final String imageUrl;
  final String title;

  learningItem(
      {required this.imageUrl, required this.title});
}
