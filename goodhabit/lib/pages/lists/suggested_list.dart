import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class suggested_list extends StatefulWidget {
  suggested_list({super.key});

  List<ActivityItem> items = [
    const ActivityItem(
        urlImage: "assets/images/activity1.png",
        title: "Walk",
        subtitle: "10 km",
        colorCode: "#FCDCD3"),
    const ActivityItem(
        urlImage: "assets/images/activity2.png",
        title: "Swim",
        subtitle: "30 min",
        colorCode: "#D7D9FF"),
    const ActivityItem(
        urlImage: "assets/images/activity3.png",
        title: "Read",
        subtitle: "20 min",
        colorCode: "#D5ECE0")
  ];

  @override
  State<suggested_list> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<suggested_list> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
      margin: const EdgeInsets.only(left: 10, right: 10),
      height: 110,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: widget.items.length,
          itemBuilder: (context, index) =>
              buildCard(item: widget.items[index])),
    ));
  }

  Widget buildCard({required ActivityItem item}) =>
      Container(
          padding: const EdgeInsets.only(
              top: 10, left: 20, right: 20),
          margin: const EdgeInsets.only(left: 7, right: 7),
          width: 170,
          decoration: BoxDecoration(
              color: HexColor(
                  item.colorCode.toString().trim()),
              borderRadius: const BorderRadius.all(
                  Radius.circular(20))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset(
                item.urlImage.toString().trim(),
                fit: BoxFit.fill,
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                item.title.toString().trim(),
                style: const TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.normal),
              ),
              const SizedBox(
                height: 1,
              ),
              Text(
                item.subtitle.toString().trim(),
                style: const TextStyle(
                    color: Colors.black45,
                    fontSize: 13,
                    fontWeight: FontWeight.normal),
              ),
            ],
          ));
}

class ActivityItem {
  final String urlImage;
  final String title;
  final String subtitle;
  final String colorCode;

  const ActivityItem(
      {required this.urlImage,
      required this.title,
      required this.subtitle,
      required this.colorCode});
}
